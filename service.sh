#!/bin/bash
#incoming parameters $1 - service_name/container_name $2 - command (start;restart;gr-restart;stop;gr-stop;kill;status)
#определение service или container
if [[ $1 = *.service ]]
then
#--- start ---
if [ "$2" = start ]
then
  systemctl start $1

#--- restart ---
elif [ "$2" = restart ]
then
  systemctl restart $1

#--- gr-restart ---
elif [ "$2" = gr-restart ]
then
  #--- если freeswitch ---
  #если $2 - freeswitch.service, тогда считаем количество сессий, если больше 0, тогда ищем ИД и убиваем сессии
  if [ $1 = "freeswitch.service" ]
  then
    fs_cli -x "fsctl shutdown restart elegant" &
    SECONDS=0
      while [[ $SECONDS -lt 3600 ]]
      do
        SCH=$(fs_cli --execute="show channels" | grep total | awk '{print $1}')
        if [ $SCH = 0 ]
        then
          break  #Завершение работы цикла
        fi
      done
  IDs=$(fs_cli --execute="show channels" | awk 'BEGIN{FS=","} {print $1}' | head -n -2 | tail -n+2)
    for ID in $IDs
    do
      fs_cli -x "uuid_kill $ID"
    done

else
  #gr-restart command $1
  systemctl restart $1
fi

  #--- stop ---
  elif [ "$2" = stop ]
  then
    systemctl stop $1

    #---gr-stop---
   elif [ "$2" = gr-stop ]
   then
     #--- если freeswitch ---
     #если $2 - freeswitch.service, тогда считаем количество сессий, если больше 0, тогда ищем ИД и убиваем сессии
     if [ $1 = "freeswitch.service" ]
     then
       fs_cli -x "fsctl shutdown elegant" &
       SECONDS=0
         while [[ $SECONDS -lt 10 ]]
         do
           SCH=$(fs_cli --execute="show channels" | grep total | awk '{print $1}')
           if [ $SCH = 0 ]
           then
             break  #Завершение работы цикла
           fi
         done
     IDs=$(fs_cli --execute="show channels" | awk 'BEGIN{FS=","} {print $1}' | head -n -2 | tail -n+2)
       for ID in $IDs
       do
         fs_cli -x "uuid_kill $ID"
       done

   else
     #gr-restart command $1
     systemctl stop $1
   fi
       #--- stop ---
       elif [ "$2" = stop ]
       then
         systemctl stop $1

       #---gr-stop---
       elif [ "$2" = gr-stop ]
       then
         #--- если freeswitch ---
         #если $2 - freeswitch.service, тогда считаем количество сессий, если больше 0, тогда ищем ИД и убиваем сессии
         #if $2 is freeswitch.service, then we count the number of sessions, if more than 0, then we look for the ID and kill the sessions
         if [ $1 = "freeswitch.service" ]
         then
           SECONDS=0
           while [ $SECONDS -lt 3600 ]
             do
               #считаем количество сессий
               #counting the number of sessions
               SCH=$(fs_cli --execute="show channels" | grep total | awk '{print $1}')
               if [ $SCH = 0 ]
               then
                 #если сессий 0, то завершаем работу сервиса
                 fs_cli -x "fsctl shutdown elegant"
                 break  #Завершение работы цикла
               fi
             done
           #если за 60мин сессий больше 0, тогда ищем ИД и убиваем сессии
           #if there are more than 0 sessions in 60 minutes, then we look for the ID and kill the session
             IDs=$(fs_cli --execute="show channels" | awk 'BEGIN{FS=","} {print $1}' | head -n -2 | tail -n+2)
               #убиваем сессии в цикле
               #killing sessions in a loop
                 for ID in $IDs
                   do
                     fs_cli -x "uuid_kill $ID"
                   done
             fs_cli -x "fsctl shutdown elegant"

           #--- если не freeswitch ---
           else
             #gr-stop command $1
             systemctl stop $1
           fi

           #--- kill ---
           elif [ "$2" = kill ]
           then
             #yum install psmisc -y
             #echo "killall $1"
             #находим pid сервиса и убиваем его
             pid=$(systemctl show --property MainPID $1 | awk 'BEGIN{FS="="} {print $2}')
             kill $pid

           #--- status ---
           elif [ "$2" = status ]
           then
             systemctl status $1
             echo " "
#             pid=$(systemctl show --property MainPID $1 | awk 'BEGIN{FS="="} {print $2}')
#             ps -T -a $pid
           fi
     #--- container ---
else
  if [ "$2" = start ]
  then
      docker-compose -f /data/installs/$1/docker-compose.yml up -d
  elif [ "$2" = restart ]
  then
      docker-compose -f /data/installs/$1/docker-compose.yml restart
  elif [ "$2" = gr-restart ]
  then
      docker-compose -f /data/installs/$1/docker-compose.yml restart -t 30
  elif [ "$2" = stop ]
  then
      docker-compose -f /data/installs/$1/docker-compose.yml stop
  elif [ "$2" = gr-stop ]
  then
      docker-compose -f /data/installs/$1/docker-compose.yml stop -t 30
  elif [ "$2" = kill ]
  then
      docker-compose -f /data/installs/$1/docker-compose.yml down
  elif [ "$2" = status ]
  then
#      top -bn1 | egrep "$1|PID"
      echo
      container_name=$(docker container ls --format '{{.Names}}' | grep $1)
      docker ps -a --format 'table {{.Names}}\t{{.Status}}\t{{.Ports}}' | egrep "$1|NAMES" 
#      echo
#      echo "Logs, last 10 lines"
#      docker logs -n10 $1
#      lastlog=$(ls -tr /data/log/$1/ | tail -n 1)
#      tail /data/log/$1/$lastlog
#      docker-compose -f /data/installs/$1/docker-compose.yml ps
  fi
fi
#--- end ---
