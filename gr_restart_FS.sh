#!/bin/bash
SCH=$(fs_cli --execute="show channels" | grep total | awk '{print $1}')

if [ $SCH = 0 ]
then
  echo "if 0 channels online"
  fs_cli -x "fsctl shutdown restart elegant"
  fs_cli -x "status"
else
  IDs=$(fs_cli --execute="show channels" | awk 'BEGIN{FS=","} {print $1}' | head -n -2 | tail -n+2)
    for ID in $IDs
      do
        echo "fs_cli -x "uuid_kill $IDs""
        fs_cli -x "uuid_kill $IDs"
      done
  echo "else fs_cli -x "fsctl shutdown restart elegant""
  fs_cli -x "fsctl shutdown restart elegant"
  fs_cli -x "status"
fi

