#!/bin/bash
echo __________________
echo АПТАЙМ СЕРВЕРА
echo
uptime
echo __________________
echo УТИЛИЗАЦИЯ КОРНЕВОГО РАЗДЕЛА ДИСКА
echo
df -h /
echo __________________
echo АКТИВНЫЕ СИСТЕМНЫЕ СЛУЖБЫ
echo
systemctl -t service --state=running --no-legend --no-pager
echo __________________
echo НЕАКТИВНЫЕ СИСТЕМНЫЕ СЛУЖБЫ
echo
systemctl -t service --state=exited --no-legend --no-pager
echo __________________
echo СТАТУС КЛАСТЕРА DOCKER SWARM
echo
docker service ls
echo __________________
echo ТЕКУЩЕЕ СОСТОЯНИЕ КОНТЕЙНЕРОВ DOCKER
echo
docker ps -a 
echo __________________